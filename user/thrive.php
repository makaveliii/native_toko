<?php include 'header.php'; ?>
     <div class="main">
      <div class="shop_top">
			<div class="row">
				
				<div class="col-md-12">
				  <div class="map">
					<img src="../images/briliant/8.jpeg" width="100%" height="70%">
				  </div>
				</div>
				<div class="col-md-12">
					<h1>TIM SURYANATION MOTORLAND BANJIR INSPIRASI DI KUNJUNGAN MUSEUM HARLEY-DAVIDSON</h1>
					
					<p class="m_8">Milwaukee, 16 Maret 2020, perjalanan Suryanation Motorland Ride to USA berlanjut setelah sebelumnya melakukan diskusi dengan beberapa builder yang hadir saat proses loading Mama Tried Show. Tak hanya berbincang, para builder yang ditemui juga memberikan apresiasi positif terhadap empat motor pemenang Suryanation Motorland 2019.</p>

					<p class="m_8">Di akhir minggu lalu, perjalanan inspiratif tim Suryanation Motorland dilanjutkan menuju salah satu destinasi wajib bagi para builder maupun pecinta roda dua yaitu Museum Harley-Davidson yang berada di Milwaukee, Winsconsin.  Museum seluas 81.000 m2 ini tidak hanya memperlihatkan sejarah lahirnya Harley-Davidson, tetapi juga akan menjelaskan inovasi dari sisi desain maupun teknologi dari setiap produk yang lahir di tahun 1903 tersebut. Sudah tentu, kunjungan ke museum ini akan semakin memperkaya pengetahuan sekaligus memberikan inspirasi kepada seluruh tim Suryanantion Motorland.</p>

					<p class="m_8">“Di Museum Harley-Davidson, saya melihat dua chopper paling terkenal, Captain America Bike dan Billy Bike. Rasanya menyenangkan banget bisa menikmati motor-motor chopper keren di negara asalnya. Saya jadi dapat banyak ilmu dan pandangan baru yang menginspirasi saya untuk bangun motor berikutnya di Indonesia,” kata Lufti Ardika, salah satu pemenang Suryanation Motorland Battle 2019 asal Surayaba.</p>

					<p class="m_8">Kunjungan tim Suryanation Motorland ke Museum Harley-Davidson juga membuahkan kejutan. Mereka bertemu motorcycle enthusiast ternama, Scott Jones dari Noise Cycle yang saat itu membawa motor flat tracker, serta Mark The Butcher dari Rusty Butcher. Tim Suryanation Motorland berkesempatan untuk berbincang dan diskusi santai dengan Scott, sekaligus menambah ilmu tentang dunia custom dari ahlinya.</p>

					<p class="m_8">“Kami memang memilih Museum Harley-Davidson sebagai salah satu tujuan perjalanan Suryanation Motorland demi memberikan kesempatan bagi pemenang Suryanation Motorland Battle 2019 untuk mendapatkan inspirasi dan ide-ide segar. Diharapkan nantinya bisa memperkaya custom scene di Indonesia,” jelas Rizky Dwianto, Suryanation Motorland Committee.</p>

					<strong>Teks: Gastank, Foto: Suryanation Motorland.</strong>
				</div>
			
			</div>
	     </div>
	   </div>
	   <?php include 'footer.php'; ?>