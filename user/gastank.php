<?php include 'header.php'; ?>
     <div class="main">
      <div class="shop_top">
			<div class="row">
				
				<div class="col-md-12">
				  <div class="map">
					<img src="../images/briliant/goodtimes-ribuan-bikers-kompak-dukung-empat-pilar-mpr-ri.jpg" width="100%" height="70%">
				  </div>
				</div>
				<div class="col-md-12">
					

					<p class="m_8">Didahului dengan Rolling Thunder, ratusan biker berkumpul di titik awal yaitu di halaman parkir Senayan City Mall, Jakarta Selatan. Tampak hadir Ketua MPR RI, Bapak Bambang Soesatyo, sebagai Road Captain. Bambang Soesatyo yang akrab disebut Bamsoet ini didampingi Bapak DR. Ir. Mochamad Basuki Hadimuljono, M.Sc, Menteri Pekerjaan Umum dan Perumahan Rakyat; Bapak DR. H. Sanitiar Burhanuddin, S.H., M.H, Jaksa Agung Republik Indonesia; Bpk Komjen. Pol. Dr. Drs. H. Mochamad Iriawan, S.H., M.M, M.H, Sekretaris Utama Lembaga Ketahanan Nasional RI; Ketua Umum Motor Besar Indonesia (MBI), Satrio Nur Rachmanto; Ketua Dewan Penasihat MBI, Kombes Pol. Dr. Putu Putera Sadana, S.I.K, M.Hum, M.M., serta Ketua Umum Harley Davidson Club Indonesia (HDCI), Komjen. Pol. (Pur) Drs. Nanan Sukarna. Ratusan biker dari Motor Besar Indonesia (MBI) dari berbagai wilayah seperti MBI DKI Jakarta, MBI Bogor, MBI Bekasi, MBI Depok, MBI Tangerang, MBI Bandung dan MBI Bali, HDCI,  Harley Owners Group (HOG) Anak Elang Jakarta Chapter, Freedom of Charge (FOC), HOG Jakarta Chapter, Police Owners Group (POG), dan lain-lain turut serta dalam rolling thunder yang berangkat dari Senayan City menuju lokasi acara yaitu halaman Gedung MPR/DPR/DPD RI, Senayan, Jakarta Pusat. Komunitas mobil seperti Lamborghini Club Indonesia, Ferrari Owners Club Indonesia dan Tesla Club Indonesia, turut menyemarakkan konvoi rolling thunder ini.</p>
				</div>
			
			</div>
	     </div>
	   </div>
	 <?php include 'footer.php'; ?>