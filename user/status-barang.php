<?php 
session_start();
if(!isset($_SESSION['email'])){
      header("location:../login/login.php");
      exit();
   }

   if(isset($_SESSION['email'])){
      $email = $_SESSION['email'];
      
   }

   include "../login/koneksi.php";
   
?>
<!DOCTYPE HTML>
<html>
<head>
<title>STREET ARTS CUSTOM</title>
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<link href="../css/reg.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="../js/jquery.min.js"></script>
<!--<script src="js/jquery.easydropdown.js"></script>-->
<!--start slider -->
<link rel="stylesheet" href="../css/fwslider.css" media="all">
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/fwslider.js"></script>
<!--end slider -->
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
</head>
<body>
	<div class="header">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
           <a href="#"><img src="../images/mm.webp" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="../images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="home.php">HOME</a></li>
						    	<li><a href="about.php">ABOUT</a></li>
						    	<li><a href="kategori.php">KATEGORI</a></li>
						    	<li><a href="galeri.php">GALERI</a></li>
						    	<li><a href="../user/keranjang.php">KERANJANG</a></li>
						    	<li><a href="../user/konfirmasi.php">KONFIRMASI</a></li>
						    	<li><a href="../login/logout.php">LOG OUT</a></li>									
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="../js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right"></div>
	      </div>
		 </div>
	    </div>
	  <div class="main">
      <div class="shop_top">
		    <div class="container">
          <div class="row">
          <div class="col-md-7">
            <div class="alert alert-info">
            <div class="table-responsive-lg">
            <table class="table">
              <form method="post" action="status-ac.php">
              <?php 

                  if (isset($_SESSION['email'])){
                    # code..
                      # code...
                  
                  $no=1;
                  $query = "SELECT * FROM  tbl_pengiriman JOIN tbl_pembelian ON 
                  tbl_pengiriman.kode_beli=tbl_pembelian.id_pembelian WHERE tbl_pembelian.id_user='$_GET[id]'";
                  $cek = mysqli_query($koneksi,$query);
                  while ($data = mysqli_fetch_array($cek)) {
                 if ($data['status']=='BELUM DITERIMA') {
                  ?>
                <tr class="danger">
                  <td>No Pembelian</td>
                  <td>:</td>
                  <td><?php echo $no;?></td>
                </tr>
                <tr>
                  <td>No Resi</td>
                  <td>:</td>
                  <td><?php echo $data['no_resi']; ?></td>
                </tr>
                 <tr>
                  <td>Kurir</td>
                  <td>:</td>
                  <td><?php echo $data['kurir']; ?></td>
                </tr>
                <tr>
                  <td>Tanggal kirim</td>
                  <td>:</td>
                  <td><?php echo $data['tgl_cfr']; ?></td>
                </tr>
                <tr>
                  <td>Id Pembelian</td>
                  <td>:</td>
                  <td><?php echo $data['id_pembelian']; ?></td>
                </tr>
                 <tr>
                  <td>Alamat Pengiriman</td>
                  <td>:</td>
                  <td><?php echo $data['alamat']; ?></td>
                </tr>
                 <tr>
                  <td>No Telpon</td>
                  <td>:</td>
                  <td><?php echo $data['no_tlp']; ?></td>
                </tr>
                 <tr>
                  <td>Kota</td>
                  <td>:</td>
                  <td><?php echo $data['kota']; ?></td>
                </tr>
                 <tr>
                  <td>Kode Pos</td>
                  <td>:</td>
                  <td><?php echo $data['kode_pos']; ?></td>
                </tr>
                 <tr>
                  <td>Status Barang</td>
                  <td>:</td>
                  <td><?php echo $data['status']; ?>&nbsp; <a href='status-ac.php?id=<?php echo $data['id_pembelian'] ?>'; class='btn btn-primary'>KONFIRMASI PENERIMAAN BARANG</a></td>
                </tr>
                <strong>Note :</strong><br />
                <i>Apabila setelah 5 hari pengiriman anda tidak melakukan konfirmasi penerimaan barang setelah barang dikirim, konfirmasi penerimaan barang akan dilakukan otomatis oleh sistem kami</i>
                
                <?php $no++; 
                  ?>


                 
                 <?php 
               }
                 }
              } 

             ?>
             
              </form>
            </table>
            </div>
             

                  
            </div>
          </div>
         </div>  
        </div>
	   </div>
	  </div>
</body>	
</html>