<?php include 'header.php'; ?>
	
<div class="banner">
	<!-- start slider -->
       <div id="fwslider">
         <div class="slider_container">
            <div class="slide"> 
                <!-- Slide image -->
               <img src="../images/briliant/9.jpg" class="img-responsive" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">Run Over<br>Everything</h1>
                        <!-- /Text title -->
                    </div>
                </div>
               <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
               <img src="../images/briliant/11.jpg" class="img-responsive" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h1 class="title">Run Over<br>Everything</h1>
                    </div>
                </div>
            </div>
            <div class="slide">
               <img src="../images/briliant/7.jpeg" class="img-responsive" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h1 class="title">Run Over<br>Everything</h1>
                    </div>
                </div>
            </div>
            <div class="slide">
               <img src="../images/briliant/12.jpg" class="img-responsive" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h1 class="title">Run Over<br>Everything</h1>
                    </div>
                </div>
            </div>
            <div class="slide">
               <img src="../images/briliant/8.jpg" class="img-responsive" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h1 class="title">Run Over<br>Everything</h1>
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
       </div>
       <!--/slider -->
      </div>
	  <div class="main">
		<div class="content-top">
			<h2>BRILLIANT CUSTOM MOTORCYCLE</h2>
			<p>FROM INDONESIA FOR THE WORLD , VINTAGE - CUSTOM MOTORCYCLE</p>
			<div class="close_but"><i class="close1"> </i></div>
				<ul id="flexiselDemo3">
				<li><img src="../images/briliant/tp.webp" /></li>
				<li><img src="../images/briliant/tp2.webp" /></li>
				<li><img src="../images/briliant/tp3.webp" /></li>
				<li><img src="../images/briliant/tp4.webp" /></li>
				<li><img src="../images/briliant/tp5.webp" /></li>
				<li><img src="../images/briliant/tp6.webp" /></li>
				<li><img src="../images/briliant/tp7.webp" /></li>
				<li><img src="../images/briliant/tp8.webp" /></li>
				
			</ul>
		<br><br><h3>SPAREPART SERIES, GET IT NOW!!</h3>
			<script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo3").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		    
		});
		</script>
		<script type="text/javascript" src="../js/jquery.flexisel.js"></script>
		</div>
	</div>
	<div class="features">
			<h3 class="m_3">Features</h3>
			<div class="close_but"><i class="close1"> </i></div>
			  <div class="row">
				<div class="col-md-3 top_box">
				  <div class="view view-ninth"><a href="tigre.php">
                    <img src="../images/briliant/goodtimes-skalput-ride-2020-the-color-of-borneo.jpg" class="img-responsive" alt="" style="width:270px;height:160px"/>
                    <div class="mask mask-1"> </div>
                    <div class="mask mask-2"> </div>
                      <div class="content">
                        <h2>BORNEO EVENT</h2>
                        <p>Event Custom Culture yang di selenggarakan pada hari Sabtu dan Minggu</p>
                      </div>
                   </a> </div
                  </div>
                  <h4 class="m_4"><a href="#">THE COLOR OF BORNEO</a></h4>
                  <p class="m_5">BORNEO EVENT</p>
                </div>
                <div class="col-md-3 top_box">
					<div class="view view-ninth"><a href="thrive.php">
                    <img src="../images/briliant/harley-davidson-logo-evolution.jpg.jfif" class="img-responsive" alt="" style="width:270px;height:160px"/>
                    <div class="mask mask-1"> </div>
                    <div class="mask mask-2"> </div>
                      <div class="content">
                        <h2 style="text-align:justify;">BANJIR INSPIRASI DI KUNJUNGAN MUSEUM HARLEY-DAVIDSON</h2>
                        <p>It’s Motorcycles, that’s what</p>
                      </div>
                    </a> </div>
                   <h4 class="m_4"><a href="#">KUNJUNGAN MUSEUM HARLEY-DAVIDSON</a></h4>
                   <p class="m_5">SURYANATION MOTORLAND</p>
				</div>
				<div class="col-md-3 top_box">
					<div class="view view-ninth"><a href="gastank.php">
                    <img src="../images/briliant/goodtimes-ribuan-bikers-kompak-dukung-empat-pilar-mpr-ri.jpg" class="img-responsive" alt="" style="width:270px;height:160px"/>
                    <div class="mask mask-1"> </div>
                    <div class="mask mask-2"> </div>
                      <div class="content">
                        <h2>BIKERS</h2>
                        <p>MPR RI, Gerak BS dan Motor Besar Indonesia mengadakan acara “Riding KebangsaanEmpat Pilar MPR RI”</p>
                      </div>
                    </a> </div>
                   <h4 class="m_4"><a href="#">EMPAT PILAR MPR RI</a></h4>
                   <p class="m_5">BIKERS</p>
				</div>
				<div class="col-md-3 top_box1">
					<div class="view view-ninth" ><a href="surya.php">
                    <img src="../images/briliant/goodtimes-riding-with-the-wind-a-motorcycle-diary-vol2.jpg" class="img-responsive" alt="" style="width:270px;height:160px"/>
                    <div class="mask mask-1"> </div>
                    <div class="mask mask-2"> </div>
                      <div class="content">
                        <h2>CUSTOM</h2>
                        <p> Riding With The Wind, a Motorcycle Diary.</p>
                      </div>
                     </a> </div>
                   <h4 class="m_4"><a href="#">MASTOM CUSTOM X DEUS EX MACHINA</a></h4>
                   <p class="m_5">CUSTOM</p>
				</div>
			</div>
		 
	    </div>
		<?php include 'footer.php'; ?>