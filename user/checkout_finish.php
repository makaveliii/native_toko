<?php require_once("cart_shop.php"); 
include '../login/koneksi.php';
// include 'header.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<link rel="icon" type="images/jpg" href="../images/briliant/logo.jpg">
<title>BRILLIANT CUSTOM MOTORCYCLE</title>
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="../js/jquery.min.js"></script>
<!--<script src="js/jquery.easydropdown.js"></script>-->
<!--start slider -->
<link rel="stylesheet" href="../css/fwslider.css" media="all">
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/fwslider.js"></script>
<!--end slider -->
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
     	<script type="text/javascript" src="../js/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.css" media="screen" />
   <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

		});
	</script>
</head>
<body>
	<div class="header">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="#"><img src="../images/mm.webp" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="../images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="home.php">HOME</a></li>
						    	<li><a href="about.php">ABOUT</a></li>
						    	<li><a href="kategori.php">KATEGORI</a></li>
						    	<li><a href="galeri.php">GALERI</a></li>
						    	<li><a href="../user/keranjang.php">KERANJANG</a></li>
						    	<li><a href="../user/konfirmasi.php">KONFIRMASI</a></li>
						    	<li><a href="../login/logout.php">LOG OUT</a></li>									
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="../js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right"></div>
	      </div>
		 </div>
	    </div>
	  <div class="main">
     <div class="container">
     	<h2>DETAIL PEMBELIAN</h2>
     	<div class="row">
		     	<div class="col-md-7">
		     		<div class="alert alert-danger">
		     		<strong style="color:black;font-weight:bold;">*Harap Salin ID BELI untuk digunakan sebagai konfirmasi pembelian!!</strong>
		     		</div>
		     	</div>
		 </div>

     	<?php
     		$query = "SELECT * FROM tbl_pembelian JOIN tbl_user 
     		ON tbl_pembelian.id_user=tbl_user.id_user 
     		WHERE tbl_pembelian.id_pembelian = '$_GET[id]'";
     		$ambil = mysqli_query($koneksi,$query);
     		$data = mysqli_fetch_array($ambil);

     	 ?>
		     			 <strong>ID BELI:<?php echo $data['id_pembelian'];?></strong><br>
				      	 <strong><?php echo $data['nama']; ?></strong><br>
				       	 <strong><?php echo $data['alamat'];?></strong><br>
				       	 <strong><?php echo $data['kota']; ?></strong><br>
				      	 <strong><?php echo $data['kode_pos']; ?></strong><br>
     	 <p>
     	 	<?php echo $data['no_tlp']; ?><br>
     	 	<?php echo $data['email']; ?>
     	 </p>
     	 Tanggal : <?php echo $data['tgl_beli'];?>
     	 Total : IDR.<?php echo number_format($data['total']); ?>
     	 </p>
		    <table class="table table-bordered"> 
		      <tr>
		        <th><center>NO</center></th>
		        <th><center>NAMA BARANG</center></th>
		        <th><center>HARGA SATUAN</center></th>
		        <th><center>JUMLAH BELI</center></th>
		        <th><center>TOTAL</center></th>
		      </tr>
		       <?php $nomor=1; ?>
		       <?php $querry = "SELECT * FROM tbl_pesanan JOIN tbl_produk
		       ON tbl_pesanan.id_barang=tbl_produk.id_barang
		       WHERE tbl_pesanan.id_pembelian='$_GET[id]'";?>
		       <?php $ambill = mysqli_query($koneksi,$querry); ?>
		       <?php while ($datax = mysqli_fetch_array($ambill)) { ?>
		       	<tr>
		       		<td><?php echo $nomor; ?></td>
		       		<td><?php echo $datax['nama_barang']; ?></td>
		       		<td><?php echo number_format($datax['harga']); ?></td>
		       		<td><?php echo $datax['jumlah_pesanan']; ?></td>
		       		<td><?php echo number_format($datax['harga']*$datax['jumlah_pesanan']); ?></td>
		       	</tr>
		       	<?php $nomor++; ?>
		       <?php 
		   }
		       ?>
		     </table>

		     <div class="row">
		     	<div class="col-md-7">
		     		<div class="alert alert-danger">
		     			<p style="color:black;">
		     				Silahkan melakukan pembayaran Rp. <?php echo number_format($data['total']); ?> Ke <br>
		     				<strong> BANK MANDIRI 025-110-3126 AN. Pt. BRILIANT CUSTOM MOTORCYCLE</strong><br>
		     				<p style="color:green;font-weight:bold;">apabila dalam waktu 2 X 24 jam anda tidak melakukan konfirmasi pembayaran, maka pesanan otomatis akan di batalkan.</p>
		     			</p>
		     		</div>
		     	</div>
		     </div>
    
     </div> 
    </div>
</body>	
</html>