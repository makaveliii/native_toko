<?php include 'header.php'; ?>
     <div class="main">
      <div class="shop_top">
			<div class="row">
				
				<div class="col-md-12">
				  <div class="map">
					<img src="../images/briliant/goodtimes-skalput-ride-2020-the-color-of-borneo.jpg" width="100%" height="70%">
				  </div>
				</div>
				<div class="col-md-12">
					<p class="m_8">Event Custom Culture yang di selenggarakan pada hari Sabtu dan Minggu (7-8/3/20) di Citra City yang bertajuk Skalput Ride 2020 menjadi acara kedua dan akan menjadi acara custom culture tahunan di kota Balikpapan.</p>

					<p class="m_8">Panitia Skalput Ride tahun ini juga lebih banyak ada Cholis (SilverFire) Nara (Bikers Brotherhood 1% MC) Ryan Nirwan (Balikpapan Ready Mix) Eddo (D'Kampretos) Bondan (Outsiders MC) dll</p>

					<p class="m_8">Berbeda dari pada tahun sebelumnya, event Skalput Ride di tahun ini bertemakan "The Color of Borneo", event ini tidak hanya menghadirkan kontes motor dan mobil classic - custom, namun juga ada banyak konten custom culture lainnya.</p>

					<p class="m_8">" Di event yang kedua ini kita mengambil tema The Color Of Borneo karena beberapa kali kita main di pulau Jawa selalu beranggapan bahwa Balikpapan khususnya Kalimantan itu masih hutan, jadi di tahun ini kita memperkenalkan warna lain dari Kalimantan bahwa kita sekarang bukan hutan lagi dan perkembangan dunia custom juga makin pesat dan itulah kenapa kita mengambil tema event ditahun ini The Color Of Borneo yang artinya warna lain dari pulau Kalimantan khususnya kota Balikpapan," Jelas Cholis sebagai ketua Skalput Ride.</p>

					<p class="m_8">Tujuan mereka membuat event Skalput Ride adalah menjadi sebuah wadah buat penikmat roda dua dan roda empat supaya bisa bergabung dan menikmati suasana custom culture, karena perkembangan pesat dunia custom diluar kota Balikpapan begitu pesat mereka juga ingin Kalimantan mempunyai sebuah event yang sama seperti diluar pulau Kalimantan, mereka juga merangkul para All Bikers Kalimantan dengan tujuan untuk menghilangkan paradigma antara komunitas motor atau mobil moderen dan komunitas motor atau mobil custom tidak bisa menyatu, tidak mengkotak - kotakkan sesama pengendara roda dua dan roda empat.</p>

					<p class="m_8">Skalput Ride 2020 Balikpapan diramaikan dengan berbagai konten seperti meet up bikers, live music, barbershop, lapak otomotif, lapak tattoo, lapak second, food, talkshow, volkers race, rolling city. Salah satu yang menarik dan menjadi sorotan di event Skalput Ride tahun ini adalah kehadiran Eddy Brokoli, Ade Habibie, Mika Motor Cycles, band Kapital ( Tenggarong ) dan Efek Rumah Kaca yang juga ikut berpartisipasi dan mengisi talkshow membahas dunia custom culture di Balikpapan.</p>

					<p class="m_8">Rolling City mereka jalankan juga rutenya lumayan panjang bersama dengan Bapak Walikota Balikpapan Rizal Effendy dan bahkan para peserta yang mengikuti Rolling City lebih banyak dari tahun kemarin ditambah dengan adanya Eddy Brokoli, Ade Habibie, dan Mika Motor Cycle jadi lebih meriah.</p>

					<p class="m_8">"Kami cuma berharap Skalput Ride bisa tumbuh besar bersama komunitas yang ada di Kalimantan, mau itu komunitas tattoo, komunitas airbrush, komunitas pasar second market, komunitas gravity, pokoknya semua komunitas kita pengennya sama - sama tumbuh dan besar bareng, karena Skalput Ride itu ada  buat mengangkat semua komunitas yang dulunya mungkin dari segi masyarakat juga belum tau dan disini ajangnya buat kita sama - sama unjuk gigi," pungkas Cholis. </p>

					<strong>Teks: Reza Foto: istimewa</strong>
				</div>
			
			</div>
	     </div>
	   </div>
	   <?php include 'footer.php'; ?>