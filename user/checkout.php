<?php require_once("cart_shop.php"); 
include '../login/koneksi.php';
// include 'header.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<link rel="icon" type="images/jpg" href="../images/briliant/logo.jpg">
<title>BRILLIANT CUSTOM MOTORCYCLE</title>
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="../js/jquery.min.js"></script>
<!--<script src="js/jquery.easydropdown.js"></script>-->
<!--start slider -->
<link rel="stylesheet" href="../css/fwslider.css" media="all">
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/fwslider.js"></script>
<!--end slider -->
<script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
     	<script type="text/javascript" src="../js/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.css" media="screen" />
   <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

		});
	</script>
</head>
<body>
	<div class="header">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="#"><img src="../images/mm.webp" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="../images/nav.png" alt="" /></a>
						    <ul class="nav" id="nav">
						    	<li><a href="home.php">HOME</a></li>
						    	<li><a href="about.php">ABOUT</a></li>
						    	<li><a href="kategori.php">KATEGORI</a></li>
						    	<li><a href="galeri.php">GALERI</a></li>
						    	<li><a href="../user/keranjang.php">KERANJANG</a></li>
						    	<li><a href="../user/konfirmasi.php">KONFIRMASI</a></li>
						    	<li><a href="../login/logout.php">LOG OUT</a></li>									
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="../js/responsive-nav.js"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right"></div>
	      </div>
		 </div>
	    </div>
	  <div class="main">
      <div class="shop_top">
		<div class="container">
    <h3>CHECKOUT</h3>
    <table class="table table-bordered">
      <tr>
        <th><center>NO PEMBELIAN</center></th>
                <th><center>ID BARANG</center></th>
        <th><center>NAMA BARNG</center></th>
        <th><center>JUMLAH BELI</center></th>
        <th><center>TOTAL</center></th>
      </tr>
       <?php
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
                
    $total = 0;
    //mysql_select_db($database_conn, $conn);
    if (isset($_SESSION['items'])) {
        foreach ($_SESSION['items'] as $key => $val) {
            $query = "SELECT * FROM tbl_produk WHERE id_barang = '$key'";
            $ambil = mysqli_query($koneksi,$query);
            $data = mysqli_fetch_array($ambil);

            $jumlah_harga = $data['harga'] * $val;
            $total += $jumlah_harga;
            $no = 1;
            ?>
                <tr>
                <td><center><?php echo $no++; ?></center></td>
                <td><center><?php echo $data['id_barang']; ?></center></td>
                <td><center><?php echo $data['nama_barang']; ?></center></td>
                <td><center><?php echo number_format($val); ?></center></td>
                <td><center><?php echo number_format($data['harga']); ?></center></td>
                </tr>
                
          <?php
                    //mysql_free_result($query);      
            }
              //$total += $sub;
            }?>  
                         <?php

          echo '
            <tr style="background-color: #DDD;"><td colspan="3" align="right"><b>Total :</b></td><td align="right"><b>Rp. '.number_format($total,2,",",".").'</b></td></td></td><td></td></tr></table>

          ';
        
        ?>
    </table>
  
		<!-- start: Container -->
			<!-- start: Table -->
                 
              <div class="title"><h1>CHECK OUT</h1></div>
               <div class="hero-unit">Harap isi form dibawah ini dengan lengkap dan benar sesuai idenditas anda!</div>
                
                <form method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                  <label>ID USER</label>
                  <input type="number" class="form-control" name="id_u" value="<?php echo $_SESSION['email']['id_user'] ?>">
                </div>
                <div class="form-group">
                  <label>NAMA LENGKAP</label>
                  <input type="text" class="form-control" name="nama" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>ALAMAT</label>
                  <input type="text" class="form-control" name="alamat" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>NO TELEPHONE</label>
                  <input type="number" class="form-control" name="tlp" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>EMAIL</label>
                  <input type="text" class="form-control" name="email" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>KOTA</label>
                  <input type="text" class="form-control" name="kota" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>KODE POS</label>
                  <input type="number" class="form-control" name="pos" autocomplete="off"> 
                 </div>
                 <div class="row">
                   <div class="col-md-4">
                     <select class="form-control" name="id_ongkir">
                       <option value="">--Pilih Ongkos Pengiriman--</option>
                       <?php
                          $cccn = "SELECT * FROM tbl_ongkir";
                          $dpn = mysqli_query($koneksi,$cccn);
                          while ($dat = mysqli_fetch_array($dpn)) {
                            ?>
                            <option value="<?php echo $dat['id_ongkir'] ?>">
                            <?php echo $dat['nama_daerah'] ?> - 
                            IDR. <?php echo number_format($dat['harga'])?>
                            </option>
                            <?php
                          }
                        ?>
                     </select>
                   </div>
                 </div>
                 
                <hr>
                <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                <a href="index.php"><input type="button" class="btn btn-danger" value="Batal"></a>
                </form>
              
              <?php
                if (isset($_POST['submit'])) {
               

                $id = $_POST['id_u'];
                $nama = $_POST['nama' ];
                $alamat = $_POST['alamat']; 
                $email = $_POST['email' ];
                $telp = $_POST['tlp' ];
                $kota = $_POST['kota'];
                $id_ongkir=$_POST['id_ongkir'];
                $pos = $_POST['pos'];

                $tgl_beli = date('Y-m-d');
                
                $ryu = "SELECT * FROM tbl_ongkir WHERE id_ongkir='$id_ongkir'";
                $dtx = mysqli_query($koneksi,$ryu);
                $tuc = mysqli_fetch_array($dtx);
                $price = $tuc['harga'];

                $total_pe = $total+ $price;
                
                if (!$id || !$nama || !$alamat || !$email || !$telp || !$kota ||!$id_ongkir ||!$pos) {
                  echo "<script> alert('MOHON LENGKA[I DATA!');
                              location = 'checkout.php'; </script>";
                }else{
                //proses pengisian data pembelian
                $simpan = mysqli_query($koneksi,"INSERT INTO tbl_pembelian(id_user,id_ongkir,nama,alamat,email,no_tlp,kota,kode_pos,tgl_beli,total) VALUES ('$id','$id_ongkir','$nama','$alamat','$email','$telp','$kota','$pos','$tgl_beli','$total_pe')");
                
                //menyimpan ke tbl pesan
                // $id_pesanan = mysqli_insert_id();
                $so = "SELECT * FROM tbl_pembelian WHERE id_user='$id' and tgl_beli = '$tgl_beli' and total ='$total_pe' ";
                $jpo = mysqli_query($koneksi,$so);
                $nn = mysqli_fetch_array($jpo);
                $pol = $nn['id_pembelian'];

                foreach ($_SESSION['items'] as $key => $val) {
                  $con = mysqli_query($koneksi,"INSERT INTO tbl_pesanan (id_pembelian,id_barang,jumlah_pesanan) VALUES ('$pol','$key','$val')");

                  $hji = "SELECT * FROM tbl_pesanan WHERE id_pembelian='$pol' ";
                  $pio = mysqli_query($koneksi,$hji);
                  $ffui = mysqli_fetch_array($pio);
                  $vfre = $ffui['id_pesanan'];
  
                  //menghapus keranjang
                  unset($_SESSION['items']);

                  
                  echo "<script>alert('pembelian sukses');</script>";
                  echo "<script>location='checkout_finish.php?id=$vfre';</script>";
                }


              }
            }
              ?>
             <!-- /. PAGE INNER  -->
            
				
			<!-- end: Table -->

		<!-- end: Container -->
				
	
		 </div>
	   </div>
	  </div>
</body>	
</html>