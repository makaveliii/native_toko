<?php include 'header.php'; ?>
     <div class="main">
      <div class="shop_top">
			<div class="row">
				
				<div class="col-md-12">
				  <div class="map">
					<img src="../images/briliant/goodtimes-riding-with-the-wind-a-motorcycle-diary-vol2.jpg" width="100%" height="70%">
				  </div>
				</div>
				<div class="col-md-12">
				

					<p class="m_8">Kehidupan bermotor telah memberikan banyak hal dalam hidup Mastom. Tidak hanya membuahkan pengalaman dalam berkendara yang selama ini dijalaninya dengan penuh obsesi dan gairah selama 27 tahun. </p>

					<p class="m_8">Namun kehidupan bermotor lah yang mampu menyempurnakan gagasan, konsep, harapan serta hayalan dan doa dalam dunia Mastom. Hal inilah yang kemudian dituangkan Mastom dalam karya-karya Seni Lukis dan Seni Grafis dalam pameran tunggalnya, yang berjudul Riding With The Wind, a Motorcycle Diary.</p>

					<p class="m_8">"Untuk pameran Riding With The Wind, a Motorcycle Diary vol.2 ini saya berkolaborasi dengan Deus Ex Machina Bali, pamerannya sendiri akan berlangsung di Deus Tample di Jl Batu Mejan, Canggu, Bali pada 21 Februari 2020 mendatang," ujar pria yang punya nama asli Tommy Dwi Djatmiko.</p>

					<p class="m_8">Ini bukan kali pertama pameran bertajuk Riding With The Wind, a Motorcycle Diary diadakan. Pada pameran Riding with the wind vol.I di Bandung, Mastom telah membawa penikmat seni ke dalam sebuah kisah pergulatan yang didalamnya terdapat mimpi-mimpi besar, doa, rasa syukur dan pencapaian dalam hidup. Cerita-cerita itu dilukiskan dengan warna-warni terang yang saling bertabrakan, komposisi yang kuat dan teks yang mendominasi pada karya.</p>

					<p class="m_8">Jika pada pameran pertama berkisah pada pergulatan aspirasinya. Maka pada pameran Riding With The Wind vol .2 penikmat seni akan dibawa kepada karya-karya yang mengisahkan gairah kehidupan bermotor itu sendiri. Kenikmatan dalam menjelajah, rasa haru persaudaraan universal dengan sesama penikmat roda dua, gelora energi untuk berkompetisi, dan hayalan liar untuk memacu kencang kendaraan impian. Pada akhirnya semua kembali kepada rasa takjub dan syukur pada daya hidup, serta harapan-harapan baru dalam penjelajahan yang tidak akan pernah berhenti, bersama angin yang terus berhembus.</p>

					<strong>Teks: Olebelo, Foto: Istimewa </strong>
				</div>
			
			</div>
	     </div>
	   </div>
	   <?php include 'footer.php'; ?>