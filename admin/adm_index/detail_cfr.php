<?php
   session_start();
   if(!isset($_SESSION['username'])){
      header("location:../akses/login.php");
      exit();
   }

   if(isset($_SESSION['username'])){
      $username = $_SESSION['username'];
   }

   include "../../login/koneksi.php";
   $query=mysqli_fetch_array(mysqli_query($koneksi,"select * from tbl_admin where username='$username'"));
   
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMIN : STREET ARTS CUSTOM</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Binary admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../akses/logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="../assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
                   <li>
                        <a class="active-menu"  href="index.php"><i class="fa fa-dashboard fa-3x"></i> HOME</a>
                    </li>
                     <li>
                        <a class="active-menu"  href="barang.php"><i class="fa fa-desktop fa-3x"></i>PRODUK</a>
                    </li>
               <li>
                        <a class="active-menu"  href="pesanan.php"><i class="fa fa-bar-chart-o fa-3x"></i>PESANAN</a>
                    </li> 
                    <li>
                        <a class="active-menu"  href="dtpenjualan.php"><i class="fa fa-sitemap fa-3x"></i>DATA PENJUALAN</a>
                    </li>         
                    <li>
                        <a class="active-menu"  href="konfirmasi.php"><i class="fa fa-desktop fa-3x"></i>KONFIRMASI BAYAR</a>
                    </li>
                    <li>
                        <a class="active-menu"  href="dt_admin.php"><i class="fa fa-bar-chart-o fa-3x"></i>DATA ADMIN</a>
                    </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
              <h2>DETAIL PENJUALAN</h2>
      <?php
        $query = "SELECT * FROM tbl_pembelian JOIN tbl_user 
        ON tbl_pembelian.id_user=tbl_user.id_user 
        WHERE tbl_pembelian.id_pembelian = '$_GET[id]'";
        $ambil = mysqli_query($koneksi,$query);
        $data = mysqli_fetch_array($ambil);

       ?>
       <strong><?php echo $data['id_pembelian'];?></strong><br>
       <strong><?php echo $data['nama']; ?></strong><br>
       <strong><?php echo $data['alamat'];?></strong><br>
       <strong><?php echo $data['kota']; ?></strong><br>
      <strong><?php echo $data['kode_pos']; ?></strong><br>
       <p>
        <?php echo $data['no_tlp']; ?><br>
        <?php echo $data['email']; ?>
       </p>
       Tanggal : <?php echo $data['tgl_beli'];?>
       Total : IDR.<?php echo number_format($data['total']); ?>
       </p>
        <table class="table table-bordered"> 
          <tr>
            <th><center>NO</center></th>
            <th><center>NAMA BARANG</center></th>
            <th><center>JUMLAH BELI</center></th>
            <th><center>TOTAL</center></th>
          </tr>
           <?php $nomor=1; ?>
           <?php $querry = "SELECT * FROM tbl_pesanan JOIN tbl_produk
           ON tbl_pesanan.id_barang=tbl_produk.id_barang
           WHERE tbl_pesanan.id_pembelian='$_GET[id]'";?>
           <?php $ambill = mysqli_query($koneksi,$querry); ?>
           <?php while ($datax = mysqli_fetch_array($ambill)) { ?>
            <tr>
              <td><?php echo $nomor; ?></td>
              <td><?php echo $datax['nama_barang']; ?></td>
              <td><?php echo $datax['jumlah_pesanan']; ?></td>
              <td><?php echo number_format($datax['harga']*$datax['jumlah_pesanan']); ?></td>
            </tr>
            <?php $nomor++; ?>
           <?php 
       }
           ?>
         </table>
         				<input type="text" readonly class="form-control" value="SUDAH DIPROSES">

             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
