<?php include 'header.php'; ?>
        <div id="page-wrapper" >
            <div id="page-inner">
              <h1>TAMBAH DATA</h1>
                <form method="post" action="proses_upload.php" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" class="form-control" name="nama">
                </div>
                <div class="form-group">
                  <label>Harga Produk (IDR)</label>
                  <input type="number" class="form-control" name="harga">
                </div>
                <div class="form-group">
                  <label>Jumlah Produk</label>
                  <input type="number" class="form-control" name="jumlah">
                </div>
                <div class="form-group">
                  <label>Foto Produk</label>
                  <input type="file" class="btn btn-warning" name="foto">
                </div>
                <div class="form-group">
                  <label>Akses</label>
                  <select name="id_cat">
                  <?php
                    include "../../login/koneksi.php";
                    $query = "SELECT * FROM tbl_kategori";
                    $ambil = mysqli_query($koneksi,$query);
                    while ($data = mysqli_fetch_array($ambil)) {
                  ?>
                  <option value="<?php echo $data['id_cat']; ?>"> <?php echo $data['kategori']; ?></option>
                                    <?php
                }
                ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Deskripsi Produk</label>
                  <textarea class="form-control" name="deskripsi"></textarea>
                </div>
                <hr>
                <input type="submit" class="btn btn-primary" value="Simpan">
                <a href="index.php"><input type="button" class="btn btn-danger" value="Batal"></a>
                </form>
            </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
