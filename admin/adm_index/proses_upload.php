<?php
// Load file koneksi.php
include "../../login/koneksi.php";

// Ambil Data yang Dikirim dari Form
$nama = $_POST['nama'];
$harga = $_POST['harga'];
$jumlah = $_POST['jumlah'];
$foto = $_FILES['foto']['name'];
$tmp = $_FILES['foto']['tmp_name'];
$akses = $_POST['id_cat'];
$deskr = $_POST['deskripsi'];
	
// Rename nama fotonya dengan menambahkan tanggal dan jam upload
$fotobaru = date('dmYHis').$foto;

// Set path folder tempat menyimpan fotonya
$path = "gambar/".$fotobaru;

// Proses upload
if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
	// Proses simpan ke Database
	$query = mysqli_query($koneksi,"INSERT INTO tbl_produk(nama_barang,harga,jumlah,gambar,id_cat,deskripsi_produk) VALUES ('$nama','$harga','$jumlah','$fotobaru','$akses','$deskr')");
	

	if($query){ // Cek jika proses simpan ke database sukses atau tidak
		// Jika Sukses, Lakukan :
		header("location: barang.php"); // Redirect ke halaman index.php
	}else{
		// Jika Gagal, Lakukan :
		echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan data ke database.";
		echo "<br><a href='tbproduk.php'>Kembali Ke Form</a>";
	}
}else{
	// Jika gambar gagal diupload, Lakukan :
	echo "Maaf, Gambar gagal untuk diupload.";
	echo "<br><a href='tbproduk.php'>Kembali Ke Form</a>";
}
?>
