<?php include 'header.php'; ?>
        <div id="page-wrapper" >
            <div id="page-inner">
              <div class="title"><h1>KONFIRMASI PENGIRIMAN</h1></div>
                
                <form method="post" action="proses_kirim.php" enctype="multipart/form-data">
                <div class="form-group">
                  <label>NO RESI</label>
                  <input type="text" class="form-control" name="resi">
                </div>
                <div class="form-group">
                  <label>KODE KONFIRMASI</label>
                  <input type="number" class="form-control" name="kode" value="<?php echo $_GET['id']; ?>">
                </div>
                <div class="form-group">
                  <label>KURIR</label>
                  <input type="text" class="form-control" name="kurir">
                </div>
                 <div class="form-group">
                  <label>STATUS</label>
                  <input type="text" readonly class="form-control" name="status1" value="BELUM DITERIMA">
                </div>
                <hr>
                <input type="submit" class="btn btn-primary" value="Submit">
                <a href="index.php"><input type="button" class="btn btn-danger" value="Batal"></a>
                </form>

             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
